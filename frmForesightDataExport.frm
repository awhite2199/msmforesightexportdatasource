VERSION 5.00
Object = "{DFD20B8E-98E4-4554-BF28-E832F9C95795}#3.0#0"; "ftp wizard3.ocx"
Begin VB.Form frmForesightDataExport 
   Caption         =   "Foresight Data export"
   ClientHeight    =   1815
   ClientLeft      =   60
   ClientTop       =   360
   ClientWidth     =   5685
   Icon            =   "frmForesightDataExport.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1815
   ScaleWidth      =   5685
   StartUpPosition =   1  'CenterOwner
   Begin FTPWIZARDLib.FTPWizard FTPWizard1 
      Left            =   120
      Top             =   120
      _Version        =   196608
      _ExtentX        =   1720
      _ExtentY        =   2381
      _StockProps     =   0
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "."
      Height          =   255
      Left            =   180
      TabIndex        =   1
      Top             =   120
      Width           =   4155
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "."
      Height          =   615
      Left            =   180
      TabIndex        =   0
      Top             =   540
      Width           =   4275
   End
End
Attribute VB_Name = "frmForesightDataExport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim aDbfs() As String
Dim nCompressedFileSize As Double, tmpFile As String
Dim strFilename As String, nFileOut As Long
Dim strFilenameIss As String, nFileOutIss As Long
Dim bTransferResult As Boolean
Private cb As Long, data As New Collection, aSub(28) As String, nEURRate As Double, nUSDRate As Double, cArchiveTypeList As String, cDigitalTypeList As String, cDigitalPlusTypeList As String, cBundleTypeList As String
Dim zip As New Chilkat_v9_5_0.ChilkatZip, ftp As New Chilkat_v9_5_0.ChilkatFtp2
Const strUPFTPHost = "internal.oaksoftware.co.uk"
'Const strUPFTPUsername = "foresight"
'Const strUPFTPPassword = "M0tor5port1942"
'Const strUPFTPDirectory = ""
Const strUPFTPUsername = "andywhite"
Const strUPFTPPassword = "ibanezZZ!"
Const strUPFTPDirectory = "/Foresight/MSM"

Private Sub Form_Load()
    Dim nFile As Integer, cDataPath As String, cSubID As String, nCount As Long, nLine As Long, i As Integer, cLine As String, nCurRec As Long, lOK As Boolean
    Dim cSubList As String, aSubs(1 To 100) As String, nSubCount As Integer, j As Integer, nccc As Long, nUpgSubValue As Double, lNoPayment As Boolean
    Dim nFile2 As Integer, nFile3 As Integer, cUDFString As String, cType As String, nDDIssues As Integer
    Dim cStartIssue As String, cEndIssue As String, cPayMethod As String, cDDCancCode As String, cProduct As String
    Dim clsLogAutoProcessMain As New clsLogAutoProcess


    
    BackColor = 12320767
    ReDim aDbfs(19) As String
    aDbfs(0) = "offer"
    aDbfs(1) = "product"
    aDbfs(2) = "sub_type"
    aDbfs(3) = "customer"
    aDbfs(4) = "subscrip"
    aDbfs(5) = "issue"
    aDbfs(6) = "source"
    aDbfs(7) = "payment"
    aDbfs(8) = "pay_proc"
    aDbfs(9) = "liabmail"
    aDbfs(10) = "currency"
    aDbfs(11) = "morderh"
    aDbfs(12) = "morderl"
    aDbfs(13) = "county"
    aDbfs(14) = "country"
    aDbfs(15) = "cust_udf"
    aDbfs(16) = "udf_cust"
    aDbfs(17) = "subtdigi"
    aDbfs(18) = "subs_aud"
    aDbfs(19) = "pay_meth"

    strFilename = App.Path & "\MSMForesightDataExportlog.txt"

    nFileOut = FreeFile
    'Check if file is in use
    Open strFilename For Append As #nFileOut
    'Export column titles
    Print #nFileOut, "----------------------------------------------------"
    Print #nFileOut, "Start: " & Now

    Set data = Nothing
    cb = code4init()
    If Dir(App.Path + "\customer.dbf") <> "" Then
        cDataPath = App.Path
    Else
        cDataPath = "D:\Oak\MSM"
    End If
    Call DbfOpen(aDbfs, cb, data, cDataPath, True, True, True)
    
    Call d4tagSelect(data("offer"), data("tOFF_PUB"))
    Call d4tagSelect(data("sub_type"), data("tSUBT_PUB"))
    Call d4tagSelect(data("customer"), data("tC_ID"))
    Call d4tagSelect(data("subscrip"), data("tS_CUSTOMER"))
    Call d4tagSelect(data("issue"), data("tISS_PUB"))
    Call d4tagSelect(data("source"), data("tSRCE_CODE"))
    Call d4tagSelect(data("payment"), data("tP_ID"))
    Call d4tagSelect(data("pay_proc"), data("tPP_PAYMENT"))
    Call d4tagSelect(data("liabmail"), data("tLM_SUBS"))
    Call d4tagSelect(data("morderh"), data("tMH_CUST"))
    Call d4tagSelect(data("morderl"), data("tML_ORDER"))
    Call d4tagSelect(data("county"), data("tCTY_CODE"))
    Call d4tagSelect(data("country"), data("tCTRY_CODE"))
    Call d4tagSelect(data("cust_udf"), data("tCUDF_CUST"))
    Call d4tagSelect(data("udf_cust"), data("tUDFC_CODE"))
    Call d4tagSelect(data("subtdigi"), data("tSUBTD_DIGI"))
    Call d4tagSelect(data("subs_aud"), data("tSAUD_FIELD"))
    Call d4tagSelect(data("pay_meth"), data("tPM_CODE"))
    
    nEURRate = 1
    nUSDRate = 1
    bTransferResult = False
    d4top (data("currency"))
    Do While d4eof(data("currency")) = 0
        If Trim(fStr("CUR_CODE")) = "USD" Then
            nUSDRate = f4double(data("CUR_RATE"))
        End If
        If Trim(fStr("CUR_CODE")) = "EUR" Then
            nEURRate = f4double(data("CUR_RATE"))
        End If
        Call d4skip(data("currency"), 1)
    Loop
    SetArchiveTypeList
    
    
    If Len(Command) > 0 And UCase(Left(Command, 9)) = "/RENEWALS" Then
        clsLogAutoProcessMain.Init
        clsLogAutoProcessMain.nProcessID = 227
        clsLogAutoProcessMain.LogAutoProcessStart
        SetPrintDigitalTypeList
        'MsgBox cArchiveTypeList
        'MsgBox cDigitalTypeList
        'MsgBox cDigitalPlusTypeList
        'MsgBox cBundleTypeList
        nFile = FreeFile
        Open cDataPath + "\ForesightData\MSM-Renewals.csv" For Output As #nFile
        Print #nFile, "Subscription ID,Customer ID,Country,Previous Sub ID,Renewed sub ID,Sub type,Offer,Print-digital,Price,Number of issues,Created date,Status,Start issue,End Issue,Source code,Source Desc,Source Type,Channel,Donor sub,Free gift,Pay method,DD Cancel Reason"
    
        Show
        
        ' subs ending after 9101 (Jan 15 issue), not foc
        nCount = 0
        Call d4tagSelect(data("subscrip"), data("tS_ID"))
        d4top (data("subscrip"))
        Do While d4eof(data("subscrip")) = 0
            'If (fStr("S_ENDISS") >= "9101  " Or (fStr("S_CONTIN") = "Y" And fStr("S_LASTSENT") > "9101  ")) And f4double(data("S_SUBAMT")) > 0 Then
            If f4double(data("S_SUBAMT")) > 0 Then
                If d4seek(data("customer"), fStr("S_CUSTOMER")) = 0 Then
                    cType = "Print"
                    If InStr(cBundleTypeList, fStr("S_SUBTYPE")) > 0 Then
                        cType = "Bundle"
                    ElseIf InStr(cDigitalPlusTypeList, fStr("S_SUBTYPE")) > 0 Then
                        cType = "DigitalPlus"
                    ElseIf InStr(cDigitalTypeList, fStr("S_SUBTYPE")) > 0 Then
                        cType = "Digital"
                    End If
                    'cStartIssue = Trim(Str(Val(Left(fStr("S_STARTISS"), 2)) - 76)) + Mid(fStr("S_STARTISS"), 3, 2)
                    cStartIssue = Trim(Str(Val(Left(fStr("S_STARTISS"), 3)) - 76)) + Mid(fStr("S_STARTISS"), 4, 2)
                    If fStr("S_CONTIN") = "Y" Then
                        If fStr("S_ACTIVE") = "X" Then
                            'cEndIssue = Trim(Str(Val(Left(fStr("S_LASTSENT"), 2)) - 76)) + Mid(fStr("S_LASTSENT"), 3, 2)
                            cEndIssue = Trim(Str(Val(Left(fStr("S_LASTSENT"), 3)) - 76)) + Mid(fStr("S_LASTSENT"), 4, 2)
                        Else
                            cEndIssue = "" ' non cancelled DD sub
                        End If
                    Else
                        'cEndIssue = Trim(Str(Val(Left(fStr("S_ENDISS"), 2)) - 76)) + Mid(fStr("S_ENDISS"), 3, 2)
                        cEndIssue = Trim(Str(Val(Left(fStr("S_ENDISS"), 3)) - 76)) + Mid(fStr("S_ENDISS"), 4, 2)
                    End If
                    If d4seek(data("offer"), fStr("S_PUBLICAT") + fStr("S_OFFER")) = 0 Then
                        cProduct = Trim(fStr("OFF_PROD"))
                    Else
                        cProduct = "NOGIFT"
                        'MsgBox "Offer not found " + fStr("S_OFFER")
                    End If
                        If d4seek(data("source"), fStr("S_SOURCE")) = 0 Then

                        End If
                    cPayMethod = "Other"
                    If d4seek(data("payment"), fStr("S_PAY_ID")) = 0 Then
                        If d4seek(data("pay_meth"), fStr("P_METHOD")) <> r4success Then
                            'MsgBox "Unable to find the pay meth: " & fStr("P_METHOD")
                        End If
                        If Trim(fStr("P_METHOD")) = "DD" Then
                            cPayMethod = "DD"
                        ElseIf Trim(fStr("P_METHOD")) = "WEBPAY" Or Trim(fStr("P_METHOD")) = "MISCCC" Or Trim(fStr("P_METHOD")) = "WEBCCC" Or Trim(fStr("P_METHOD")) = "WEBMOT" Or Trim(fStr("PM_TYPE")) = "CC" Or Trim(fStr("PM_TYPE")) = "DC" Or Trim(fStr("P_METHOD")) = "JELLYF" Then
                            If d4seek(data("pay_meth"), fStr("P_METHOD")) = r4success Then
                                'credit/debit card
                                If fStr("S_CONTIN") = "Y" Then
                                    cPayMethod = "CCC"
                                Else
                                    cPayMethod = "CC"
                                End If
                            Else
                                'Other
                            End If
                        End If
                    End If
                    cDDCancCode = ""
                    If cPayMethod = "DD" And fStr("S_ACTIVE") = "X" Then
                        Call d4seek(data("subs_aud"), fStr("S_ID") + "S_STATUS  ")
                        Do While fStr("S_ID") + "S_STATUS  " = fStr("SAUD_SUBS") + fStr("SAUD_FIELD")
                            If Trim(fStr("SAUD_NEW")) = "CANCEL" And IsBlank(cDDCancCode) Then
                                cDDCancCode = Trim(fStr("SAUD_REAS"))
                            ElseIf Trim(fStr("SAUD_NEW")) = "FUTCAN" Then
                                cDDCancCode = Trim(fStr("SAUD_REAS"))
                            End If
                            Call d4skip(data("subs_aud"), 1)
                        Loop
                    End If
                    If cPayMethod <> "DD" And cPayMethod <> "CCC" Then
                        If d4seek(data("pay_proc"), fStr("S_PAY_ID")) = 0 Then
                            If Left(fStr("PP_STATUS"), 1) = "A" Then
                                Print #nFile, fStr("S_ID") + "," + fStr("S_CUSTOMER") + "," + Trim$(fStr("C_COUNTRY")) + "," + Trim(fStr("S_RENEWOF")) + "," + Trim$(fStr("S_RENEWID")) + "," + Trim$(fStr("S_SUBTYPE")) + "," + Trim(fStr("S_OFFER")) + "," + cType + "," + Trim$(Str(f4double(data("S_SUBAMT")))) + "," + Trim$(fStr("S_NOISSUES")) + "," + fStr("S_CREATED") + "," + fStr("S_STATUS") + "," + cStartIssue + "," + cEndIssue + "," + Trim$(fStr("S_SOURCE")) + "," + NoComma(fStr("SRCE_DESC")) + "," + Trim$(fStr("SRCE_TYPE")) + "," + Trim$(fStr("S_UDF1")) + "," + IIf(IsBlank(fStr("S_DONORID")), "N", "Y") + "," + cProduct + "," + cPayMethod + "," + cDDCancCode
                            End If
                        End If
                    Else
                        nDDIssues = 12
                        Select Case Trim(fStr("P_RECURFRQ"))
                        Case "M"
                            nDDIssues = 1
                        Case "Q"
                            nDDIssues = 3
                        Case "S"
                            nDDIssues = 6
                        End Select
                        Call d4seek(data("pay_proc"), fStr("S_PAY_ID"))
                        Do While fStr("PP_PAYMENT") = fStr("S_PAY_ID")
                            If Left(fStr("PP_STATUS"), 1) = "A" Or Left(fStr("PP_STATUS"), 1) = "P" Then
                                'A, P
                                Print #nFile, fStr("S_ID") + "," + fStr("S_CUSTOMER") + "," + Trim$(fStr("C_COUNTRY")) + "," + Trim(fStr("S_RENEWOF")) + "," + Trim$(fStr("S_RENEWID")) + "," + Trim$(fStr("S_SUBTYPE")) + "," + Trim(fStr("S_OFFER")) + "," + cType + "," + Trim$(Str(f4double(data("PP_AMOUNT")))) + "," + Trim$(Str(nDDIssues)) + "," + fStr("PP_DATE") + "," + Trim(fStr("PP_STATUS")) + "," + cStartIssue + "," + cEndIssue + "," + Trim$(fStr("S_SOURCE")) + "," + NoComma(fStr("SRCE_DESC")) + "," + Trim$(fStr("SRCE_TYPE")) + "," + Trim$(fStr("S_UDF1")) + "," + IIf(IsBlank(fStr("S_DONORID")), "N", "Y") + "," + cProduct + "," + cPayMethod + "," + cDDCancCode
                            ElseIf (Trim(fStr("PP_STATUS")) = "" Or Left(fStr("PP_STATUS"), 2) = "RS" Or Left(fStr("PP_STATUS"), 2) = "RC" Or Left(fStr("PP_STATUS"), 1) = "C" Or Left(fStr("PP_STATUS"), 1) = "S") And fStr("PP_DATE") <= Format(Date, "YYYYMMDD") Then
                                'C, RC, S,empty,RS
                                Print #nFile, fStr("S_ID") + "," + fStr("S_CUSTOMER") + "," + Trim$(fStr("C_COUNTRY")) + "," + Trim(fStr("S_RENEWOF")) + "," + Trim$(fStr("S_RENEWID")) + "," + Trim$(fStr("S_SUBTYPE")) + "," + Trim(fStr("S_OFFER")) + "," + cType + "," + Trim$(Str(f4double(data("PP_AMOUNT")))) + "," + Trim$(Str(nDDIssues)) + "," + fStr("PP_DATE") + "," + Trim(fStr("PP_STATUS")) + "," + cStartIssue + "," + cEndIssue + "," + Trim$(fStr("S_SOURCE")) + "," + NoComma(fStr("SRCE_DESC")) + "," + Trim$(fStr("SRCE_TYPE")) + "," + Trim$(fStr("S_UDF1")) + "," + IIf(IsBlank(fStr("S_DONORID")), "N", "Y") + "," + cProduct + "," + cPayMethod + "," + cDDCancCode
                            End If
                            '
                            Call d4skip(data("pay_proc"), 1)
                        Loop
                    End If
                Else
                    'MsgBox "Customer " + fStr("S_CUSTOMER") + " not found"
                End If
            Else
                'free subs
            End If
            nCount = nCount + 1
            If nCount / 1000 = Round(nCount / 1000) Then
                DoEvents
            End If
            Label1 = fStr("S_ID")
            Label1.Refresh
            Call d4skip(data("subscrip"), 1)
        Loop
        Close #nFile
        Label2 = "Finished"
        'Compress
        Call zipfiles(cDataPath + "\ForesightData\", "MSM-Renewals.csv")
        'FTP File
        If Dir(cDataPath & "\ForesightData\ForesightData.zip") <> "" Then
            'Try 10x
            For i = 0 To 10
                bTransferResult = TransferFile(cDataPath + "\ForesightData\", "MSM-Renewals.zip")
                If bTransferResult Then
                    Exit For
                End If
            Next i
        End If
        If bTransferResult Then
            clsLogAutoProcessMain.cProcessResult = "Success"
            clsLogAutoProcessMain.cProcessNotes = cDataPath & "\ForesightData\MSM-Renewals.zip transferred."
            clsLogAutoProcessMain.LogAutoProcessFinish
        Else
            clsLogAutoProcessMain.cProcessResult = "Failure"
            clsLogAutoProcessMain.cProcessNotes = cDataPath & "\ForesightData\MSM-Renewals.zip not transferred."
            clsLogAutoProcessMain.LogAutoProcessFinish
        End If
        
        
        
    Else
        clsLogAutoProcessMain.Init
        clsLogAutoProcessMain.nProcessID = 228
        clsLogAutoProcessMain.LogAutoProcessStart
        
        nFile = FreeFile
        Open cDataPath + "\ForesightData\ForesightData.csv" For Output As #nFile
        Print #nFile, "Subcription ID,Start issue date,Last issued received date,Original source type,Original source code,Original Source desc,Original payment method,Current payment method,Type (paid or free),Recipient/Donor,Issues received to date,DD term,Acquisition date,Expiry issue date,Current payment frequency,Current payment amount,UK/USA/ROW,Latest Subscription ID,FuturepayID,CCC,Original start issue,Latest start issue,Latest print,Latest digital,Latest print value,Latest digital value,Latest bundle value,Latest archive,Latest archive value,Customer ID"
        nFileOutIss = FreeFile
        Open cDataPath + "\ForesightData\IssueData.csv" For Output As #nFileOutIss
        Print #nFileOutIss, "Publication,Issue No,Issue Desc,Mail Date,On Sale Date"
    
        Show
        
        ' aSub(0)=Subscription URN - This will be the unique subscription ID of the 1st subscription in the chain
        ' aSub(1)=Start issue date - This is the mailing date of the 1st issue in the 1st subscription
        ' aSub(2)=Last issue received date - This is the mailing date of the issue last mailed as part of this subscription
        ' aSub(3)=Original source channel (e.g online, inserts, third parties, off the page etc) - This will be the source type for the source code of the 1st subscription
        ' aSub(4)=Original Source code (a more detailed field than above) - This will be the source code of the 1st subscription
        ' aSub(5 + 6)=Original and current payment methods (Cash, DD) - 2 columns: the payment method code for the 1st subscription and the payment method of the most recent in the chain (where there is only 1 subscription, these will be the same)
        ' aSub(7)=Subscriber Type (paying sub or free subscription) - Based on subscription value being >�0 or not
        ' aSub(8)=Recipient/Donor (is this a gift for someone else or a private subscription)
        ' aSub(9)=Number of issues received (issues received to date) - This will be the total issues recevied as part of the subscription (note this may not go back much further than when we imported data from a previous system)
        ' aSub(10)=Direct Debit term (to identify 3,6,12 issue DD�s) - May also include 2 & 3 year DD terms
        ' aSub(11)=Acquisition Date - Date of very 1st subscription for this customer. This may not be the date when this particular subscription was initally created
        ' aSub(12)=Expiry Issue Date - Date of mailing for the last issue. Within our system, continuous subscriptions eg DD, do not have an expiry issue. However, we will determine the date of the last issue that will be sent before the next DD payment is scheduled
        ' aSub(13)=Current Payment frequency
        ' aSub(14)=Current payment amount - For DDs, this is the amount they pay each time
        ' aSub(15)=UK/USA/ROW
        ' aSub(16)=Latest sub ID
        ' aSub(17)=Futurepay ID
        ' aSub(18)=continuous credit card
        ' aSub(19)=Original sub start issue
        ' aSub(20)=Latest sub start issue
        ' aSub(21)=Latest sub print Y/N
        ' aSub(22)=Latest sub digital Y/N
        ' aSub(23)=Latest print �
        ' aSub(24)=Latest digital �
        ' aSub(25)=Latest bundle �
        ' aSub(26)=Latest archive - Y/N - for bundle & digital+
        ' aSub(27)=Latest archive value � - digital plus
        ' aSub(28)=customer ID
        
        d4top (data("issue"))
        Do While d4eof(data("issue")) = 0
            Print #nFileOutIss, fStr("ISS_PUB") & "," & fStr("ISS_NUMBER") & "," & fStr("ISS_DESC") & "," & fStr("ISS_MAIL") & "," & fStr("ISS_ONSALE")
            Call d4skip(data("issue"), 1)
        Loop
        
        nCount = 0
        nLine = 0
        nccc = 0
        d4top (data("customer"))
        Do While d4eof(data("customer")) = 0
            If d4seek(data("subscrip"), fStr("C_ID")) = 0 Then
                nSubCount = 0
                nUpgSubValue = 0
                Do While fStr("C_ID") = fStr("S_CUSTOMER") And d4eof(data("subscrip")) = 0
                    nSubCount = nSubCount + 1
                    aSubs(nSubCount) = fStr("S_ID")
                    Call d4skip(data("subscrip"), 1)
                Loop
                Call d4tagSelect(data("subscrip"), data("tS_ID"))
                cSubList = ""
                For j = nSubCount To 1 Step -1
                    Call d4seek(data("subscrip"), aSubs(j))
                    If InStr(cSubList, fStr("S_ID")) = 0 Then
                        cSubList = cSubList + fStr("S_ID") + "~"
                        For i = 0 To 28
                            aSub(i) = ""
                        Next i
                        aSub(0) = fStr("S_ID")
                        aSub(16) = fStr("S_ID")
                        aSub(28) = fStr("S_CUSTOMER")
                        aSub(1) = GetIssueDate("Start")
                        aSub(2) = GetIssueDate("LastSent") ' may be changed if there's a renewal sub
                        If d4seek(data("source"), fStr("S_SOURCE")) = 0 Then
                            aSub(3) = fStr("SRCE_TYPE")
                        Else
                            aSub(3) = "UNKNWN"
                        End If
                        aSub(4) = fStr("S_SOURCE")
                        lNoPayment = True
                        If IsBlank(fStr("S_PAY_ID")) Then
                            aSub(5) = "NONE"
                        ElseIf fStr("S_PAY_ID") = "000000" Then
                            aSub(5) = "FOC"
                        ElseIf d4seek(data("payment"), fStr("S_PAY_ID")) = 0 Then
                            aSub(5) = fStr("P_METHOD")
                            lNoPayment = False
                        Else
                            aSub(5) = "UNKNWN"
                        End If
                        aSub(6) = aSub(5) ' may be changed if there's a renewal sub
                        aSub(7) = IIf(f4double(data("S_SUBAMT")) > 0, "Paid", "Free")
                        aSub(8) = IIf(IsBlank(fStr("S_DONORID")), "Recipient", "Donor")
                        If fStr("S_ACTIVE") = "X" Or fStr("S_CONTIN") = "Y" Then
                            ' cancelled so count issues sent
                            aSub(9) = Str(CountIssuesSent)
                        Else
                            aSub(9) = f4int(data("S_NOISSUES")) - f4int(data("S_ISS_LEFT")) ' may be changed if there's a renewal sub
                        End If
                        aSub(11) = f4date(data("S_CREATED"))
                        aSub(12) = GetIssueDate("End") ' may be changed if there's a renewal sub
                        If lNoPayment Then
                            ' sub not paid or no payment record found
                            aSub(10) = ""
                            aSub(13) = Trim(fStr("S_NOISSUES"))
                            aSub(14) = "0"
                            aSub(17) = ""
                        Else
                            aSub(10) = IIf(Trim(fStr("P_METHOD")) = "DD", GetDDTerm(fStr("P_RECURFRQ")), "")
                            aSub(13) = IIf(Trim(fStr("P_METHOD")) = "DD", aSub(10), Trim(fStr("S_NOISSUES")))
                            If d4seek(data("pay_proc"), fStr("P_ID")) = 0 Then
                                If Trim(fStr("P_METHOD")) = "DD" Then
                                    ' use PP amount for DD
                                    aSub(14) = Trim(Str(f4double(data("PP_AMOUNT"))))
                                Else
                                    aSub(14) = Trim(Str(f4double(data("S_SUBAMT"))))
                                End If
                                'If Trim(fStr("P_CURR")) = "UKP" Or Trim(fStr("P_METHOD")) = "DD" Then
                                '    ' use PP amount for UKP or if DD
                                '    aSub(14) = Trim(Str(f4double(data("PP_AMOUNT"))))
                                'Else
                                '    ' use p_amount which is sterling value - can't do this as it may include mail order records
                                '    'aSub(14) = Trim(Str(f4double(data("P_AMOUNT"))))
                                '    aSub(14) = Trim(Str(f4double(data("S_SUBAMT"))))
                                'End If
                            Else
                                aSub(14) = "0"
                            End If
                            aSub(17) = Trim(fStr("P_UDF2"))
                        End If
                        aSub(15) = IIf(Trim(fStr("C_COUNTRY")) = "UK", "UK", IIf(Trim(fStr("C_COUNTRY")) = "USA", "USA", "ROW"))
                        aSub(18) = IIf(IsBlank(aSub(17)), "N", "Y")
                        aSub(19) = Trim(fStr("S_STARTISS"))
                        aSub(20) = Trim(fStr("S_STARTISS"))
                        'Call d4seek(data("sub_type"), fStr("S_PUBLICAT") + fStr("S_SUBTYPE"))
                        'aSub(21) = IIf(fStr("SUBT_DIGIT") = "Y" And fStr("SUBT_PRDIG") <> "Y", "N", "Y") ' print sub
                        'aSub(22) = IIf(fStr("SUBT_DIGIT") = "Y" Or fStr("SUBT_PRDIG") = "Y", "Y", "N")
                        SetSubTypes
                        SetSubValues
                        ' check if there's a renewal sub
                        If Not IsBlank(fStr("S_RENEWID")) Then
                            lOK = True
                            Do While Not IsBlank(fStr("S_RENEWID")) And lOK
                                If d4seek(data("subscrip"), fStr("S_RENEWID")) = 0 Then
                                    If InStr(cSubList, fStr("S_ID")) = 0 Then
                                        ' found renewal - check if another renewal
                                        aSub(27) = IIf(IsBlank(fStr("S_DONORID")), "Recipient", "Donor")
                                        aSub(2) = IIf(IsBlank(fStr("S_LASTSENT")), aSub(2), GetIssueDate("LastSent"))
                                        lNoPayment = True
                                        If IsBlank(fStr("S_PAY_ID")) Then
                                            aSub(6) = "NONE"
                                        ElseIf fStr("S_PAY_ID") = "000000" Then
                                            aSub(6) = "FOC"
                                        ElseIf d4seek(data("payment"), fStr("S_PAY_ID")) = 0 Then
                                            lNoPayment = False
                                            aSub(6) = fStr("P_METHOD")
                                            If IsBlank(aSub(17)) Then
                                                aSub(17) = Trim(fStr("P_UDF2"))
                                                aSub(18) = IIf(IsBlank(aSub(17)), "N", "Y")
                                            End If
                                        Else
                                            aSub(6) = "UNKNWN"
                                        End If
                                        If fStr("S_ACTIVE") = "X" Then
                                            ' cancelled so count issues sent
                                            aSub(9) = Str(Int(aSub(9)) + CountIssuesSent)
                                        Else
                                            aSub(9) = Str(Int(aSub(9)) + f4int(data("S_NOISSUES")) - f4int(data("S_ISS_LEFT")))
                                        End If
                                        aSub(12) = GetIssueDate("End")
                                        If lNoPayment Then
                                            aSub(13) = Trim(fStr("S_NOISSUES"))
                                            aSub(14) = "0"
                                        Else
                                            aSub(10) = IIf(Trim(fStr("P_METHOD")) = "DD", GetDDTerm(fStr("P_RECURFRQ")), "")
                                            aSub(13) = IIf(Trim(fStr("P_METHOD")) = "DD", aSub(10), Trim(fStr("S_NOISSUES")))
                                            If d4seek(data("pay_proc"), fStr("P_ID")) = 0 Then
                                                If Trim(fStr("P_METHOD")) = "DD" Then
                                                    ' use PP amount for DD
                                                    aSub(14) = Trim(Str(f4double(data("PP_AMOUNT"))))
                                                Else
                                                    aSub(14) = Trim(Str(f4double(data("S_SUBAMT"))))
                                                End If
                                                'If Trim(fStr("P_CURR")) = "UKP" Then
                                                '    aSub(14) = Trim(Str(f4double(data("PP_AMOUNT"))))
                                                'Else
                                                '    ' use p_amount which is sterling value
                                                '    'aSub(14) = Trim(Str(f4double(data("P_AMOUNT"))))
                                                '    aSub(14) = Trim(Str(f4double(data("S_SUBAMT"))))
                                                'End If
                                            Else
                                                aSub(14) = "0"
                                            End If
                                        End If
                                        aSub(16) = fStr("S_ID")
                                        aSub(20) = Trim(fStr("S_STARTISS"))
                                        'Call d4seek(data("sub_type"), fStr("S_PUBLICAT") + fStr("S_SUBTYPE"))
                                        'aSub(21) = IIf(fStr("SUBT_DIGIT") = "Y" And fStr("SUBT_PRDIG") <> "Y", "N", "Y")
                                        'aSub(22) = IIf(fStr("SUBT_DIGIT") = "Y" Or fStr("SUBT_PRDIG") = "Y", "Y", "N")
                                        SetSubTypes
                                        SetSubValues
                                        nCount = nCount + 1
                                        cSubList = cSubList + fStr("S_ID") + "~"
                                    Else
                                        lOK = False
                                    End If
                                End If
                            Loop
                        End If
                        
                        ' export sub
                        cLine = ""
                        For i = 0 To 28
                            cLine = cLine + Trim(aSub(i)) + IIf(i = 28, "", ",")
                        Next i
                        
                        'If aSub(18) = "Y" And (Mid(aSub(12), 7, 4) > "2012" Or (Mid(aSub(12), 7, 4) = "2012" And Mid(aSub(12), 4, 2) >= "11")) Then
                        '    nccc = nccc + 1
                        'End If
                        
                        Print #nFile, cLine
                        
                        nLine = nLine + 1
                        nCount = nCount + 1
                    End If
                Next j
                Call d4tagSelect(data("subscrip"), data("tS_CUSTOMER"))
            End If
           
            Label1 = fStr("C_ID")
            Label1.Refresh
            Call d4skip(data("customer"), 1)
        Loop
        
        Label2 = Str(nLine) + "," + Str(nCount) + ", " + Str(d4recCount(data("subscrip")))
        
        Close #nFile
        Close #nFileOutIss
        'Compress
        Call zipfiles(cDataPath + "\ForesightData\", "ForesightData.csv")
        'Compress
        Call zipfiles(cDataPath + "\ForesightData\", "IssueData.csv")
        
           
        'Transfer to FTP.
        If Dir(cDataPath + "\ForesightData\ForesightData.zip") <> "" Then Call TransferFile(cDataPath + "\ForesightData\", "IssueData.zip")
        If Dir(cDataPath + "\ForesightData\ForesightData.zip") <> "" Then
            'Try 10x
            For i = 0 To 10
                bTransferResult = TransferFile(cDataPath + "\ForesightData\", "ForesightData.zip")
                If bTransferResult Then
                    Exit For
                End If
            Next i
        End If
        If bTransferResult Then
            clsLogAutoProcessMain.cProcessResult = "Success"
            clsLogAutoProcessMain.cProcessNotes = cDataPath & "\ForesightData\ForesightData.zip transferred."
            clsLogAutoProcessMain.LogAutoProcessFinish
        Else
            clsLogAutoProcessMain.cProcessResult = "Failure"
            clsLogAutoProcessMain.cProcessNotes = cDataPath & "\ForesightData\ForesightData.zip not transferred."
            clsLogAutoProcessMain.LogAutoProcessFinish
        End If

    End If
    
    Close #nFileOut
    
    Unload Me

End Sub
Private Function NoComma(ByVal cString As String)
    Dim i As Integer, cChar As String
    NoComma = ""
    cString = AllTrim(cString)
    For i = 1 To Len(cString)
        cChar = Mid(cString, i, 1)
        If cChar = "," Or cChar = Chr(13) Or cChar = Chr(10) Then
        Else
            NoComma = NoComma + cChar
        End If
    Next i
End Function
Sub TransferFileOld(cPath As String, cFile As String)
'Const strUPFTPHost = "www.oaksoftware.co.uk"
'Const strUPFTPUsername = "u50197779-andywhite"
'Const strUPFTPPassword = "1b4n3zZZ"
'Const strUPFTPDirectory = "Storage/OakFiles/OakBackup/"
    
    Label2 = ("Opening Internet Connection")
    FTPWizard1.UnlockFTPWizard "GUQE1411166114974836"
    Label2.Refresh
    If (FTPWizard1.OpenInternetConnection(FTP_CONNECTION_DIRECT, "")) Then
        Label2 = ("Opening FTP Site " & strUPFTPHost)
        Label2.Refresh
        Print #nFileOut, ("Opening FTP Site " & strUPFTPHost)
        If (FTPWizard1.OpenFTPConnection(strUPFTPHost, strUPFTPUsername, strUPFTPPassword)) Then
            If Len(strUPFTPDirectory) > 0 Then
                If FTPWizard1.SetCurrentDirectory(strUPFTPDirectory) Then
                    'Directory set

                Else
                    Label2 = "Error setting directory to " & strUPFTPDirectory
                    Label2.Refresh
                    Print #nFileOut, "Error setting directory to " & strUPFTPDirectory
                End If

            End If
            Label2 = "Ready."
            Label2.Refresh
            'Transfer file
            Label2 = "Beginning Transfer"
            Label2.Refresh
            Print #nFileOut, "Beginning Transfer"
            
            If Not FTPWizard1.UploadFTPFileWithControl(cPath & cFile, cFile, True) Then
                Label2 = ("FTP File transfer error:" & FTPWizard1.GetlastErrorDescription)
                Label2.Refresh
                Print #nFileOut, "FTP File transfer error:" & FTPWizard1.GetlastErrorDescriptio
            Else
                Print #nFileOut, cFile & " Transfer complete."
                Print #nFileOut, "End: " & Now
            End If
            Label2 = "Deleting Temporary Files"
            Label2.Refresh
            'Kill tmpfile
            Label2 = "Done."
            Label2.Refresh
        Else
            Label2 = ("FTP Site connect error:" & FTPWizard1.GetlastErrorDescription)
            Label2.Refresh
            Print #nFileOut, ("FTP Site connect error:" & FTPWizard1.GetlastErrorDescription)
        End If
    Else
        Label2 = ("Internet open error:" & FTPWizard1.GetlastErrorDescription)
        Label2.Refresh
        Print #nFileOut, "Internet open error:" & FTPWizard1.GetlastErrorDescription
    End If
    FTPWizard1.CloseInternetConnection
End Sub
Function TransferFile(cPath, cFile As String) As Boolean
    '  Important: It is helpful to send the contents of the
    '  sftp.LastErrorText property when requesting support.
    TransferFile = False
    Dim sftp As New ChilkatSFtp
    
    '  Any string automatically begins a fully-functional 30-day trial.
    Dim success As Long
    success = sftp.UnlockComponent("OAKSFT.CB1022020_ddzCMuHRne55") '"STVSTR.CB10216_GBzbgtf0lH9e") 'UK2NETSSH_RsU9MgnZpRnj")
    If (success <> 1) Then
        Print #nFileOut, sftp.LastErrorText
        Label2 = sftp.LastErrorText
        Label2.Refresh
        Exit Function
    End If
    
    '  Set some timeouts, in milliseconds:
    sftp.ConnectTimeoutMs = 5000
    sftp.IdleTimeoutMs = 15000
    
    '  Connect to the SSH server.
    '  The standard SSH port = 22
    '  The hostname may be a hostname or IP address.
    Dim port As Long
    'hostname = "oakwebservices.co.uk"
    port = 22
    success = sftp.Connect(strUPFTPHost, port)
    If (success <> 1) Then
        MsgBox sftp.LastErrorText
        Exit Function
    End If

    '  Authenticate with the SSH server.  Chilkat SFTP supports
    '  both password-based authenication as well as public-key
    '  authentication.  This example uses password authenication.

    
    success = sftp.AuthenticatePw(strUPFTPUsername, strUPFTPPassword)
    If (success <> 1) Then
        Print #nFileOut, sftp.LastErrorText
        Label2 = sftp.LastErrorText
        Label2.Refresh
        Exit Function
    End If
    
    '  After authenticating, the SFTP subsystem must be initialized:
    success = sftp.InitializeSftp()
    If (success <> 1) Then
        Print #nFileOut, sftp.LastErrorText
        Label2 = sftp.LastErrorText
        Label2.Refresh
        Exit Function
    End If
    
    '  Open a file for writing on the SSH server.
    '  If the file already exists, it is overwritten.
    '  (Specify "createNew" instead of "createTruncate" to
    '  prevent overwriting existing files.)
    Dim handle As String
    'handle = sftp.OpenFile("/var/www/key/shop/docs/images/" + cFileName, "writeOnly", "createTruncate")
    handle = sftp.OpenFile(strUPFTPDirectory & "/" & cFile, "writeOnly", "createTruncate")
    If (handle = vbNullString) Then
        Print #nFileOut, sftp.LastErrorText
        Label2 = sftp.LastErrorText
        Label2.Refresh
        Exit Function
    End If
    
    '  Upload from the local file to the SSH server.
    success = sftp.UploadFile(handle, cPath & cFile)
    If (success <> 1) Then
        Print #nFileOut, sftp.LastErrorText
        Label2 = sftp.LastErrorText
        Label2.Refresh
        Exit Function
    End If
    
    '  Close the file.
    success = sftp.CloseHandle(handle)
    If (success <> 1) Then
        Print #nFileOut, sftp.LastErrorText
        Label2 = sftp.LastErrorText
        Label2.Refresh
        Exit Function
    End If

    Print #nFileOut, "Upload " & cPath & cFile & " Success."
    Label2 = "Success."
    Label2.Refresh
    TransferFile = True
End Function
Function StripExtension(strpath)
    StripExtension = Mid(strpath, 1, InStr(1, strpath, ".") - 1)
End Function

Private Sub zipfiles(cPath As String, cFile As String)
    Dim nPos As Integer, nResult As Long, lOK As Boolean, nRecurse As Long, cFileList As String, cSA As New CkStringArray
    
    lOK = True
    '  Any string unlocks the component for the 1st 30-days.
    nResult = zip.UnlockComponent("UK2NETZIP_L8rzKGX8n0p8")
    If (nResult <> 1) Then
        Label2 = "Unable to unlock zip component:" + zip.LastErrorText
        Print #nFileOut, "Unable to unlock zip component:" + zip.LastErrorText
        Label2.Refresh
        lOK = False
    End If

    If lOK Then
        nResult = zip.NewZip(cPath + StripExtension(cFile) + ".zip")
        If (nResult <> 1) Then
            Label2 = "Unable to create new zip file:" + zip.LastErrorText
            Label2.Refresh
            Print #nFileOut, "Unable to create new zip file:" + zip.LastErrorText
            lOK = False
        End If
    End If
    
    If lOK Then
       
        zip.DiscardPaths = 1
        '  Set properties to indicate that the Zip should be AES encrypted. A value of 4 indicates WinZip compatible AES encryption.
        zip.Encryption = 0 ' not encrypted
        '  Key length can be 128, 192, or 256 bits.
        'zip.EncryptKeyLength = 256
        zip.PasswordProtect = 2 ' use password protection as aes encrypted
        zip.EncryptPassword = "St4mford#2020"
        

        ' in this case, we want all the ones in the current directory
        nResult = zip.AppendFiles(cPath & cFile, 0)
        If nResult <> 1 Then
            lOK = False
            Label2 = "Unable to create add files " + cPath & cFile & "): " + zip.LastErrorText
            Label2.Refresh
            Print #nFileOut, "Unable to create add files " + cPath & cFile & "): " + zip.LastErrorText
        End If
       
        If lOK Then
            ' actually generate zip
            Label2 = "Creating " + StripExtension(cFile) + ".zip"
            Label2.Refresh

            nResult = zip.WriteZip()
            If (nResult <> 1) Then
                Label2 = "Zip error " + zip.LastErrorText
                Label2.Refresh
                Print #nFileOut, "Zip error " + zip.LastErrorText
                lOK = False
            Else
                ' zip OK
                Label2 = "Zip OK"
                Label2.Refresh
                Print #nFileOut, "Zip OK"
            End If
        End If
        zip.CloseZip
    End If
    nCompressedFileSize = FileLen(cPath & StripExtension(cFile) & ".zip")
    Print #nFileOut, "Compressed file size " & StripExtension(cFile) & ".zip" & " - " & FormatFileSize(nCompressedFileSize)
End Sub
Private Sub FTPWizard1_FileTransferStatus(ByVal sRemoteFileName As String, ByVal sLocalFilename As String, ByVal dBytesTransferred As Long)
    Label2.caption = "Total bytes transferred: " & FormatFileSize(CDbl(dBytesTransferred)) & " ( " & Round((dBytesTransferred / (nCompressedFileSize)) * 100, 0) & "% )"
    Label2.Refresh
End Sub
Function FormatFileSize(nSize As Double)
    If nSize / 1024 > 1024 Then
        FormatFileSize = Round(nSize / 1024 / 1024, 2) & " Mb"
    Else
        FormatFileSize = Round(nSize / 1024, 2) & " Kb"
    End If
End Function
Private Function GetFieldData(cField As String, Optional lLastField As Boolean)
    If cField = "C_COUNTY" Then
        If Not IsBlank(fStr(cField)) Then
            Call d4seek(data("county"), fStr(cField))
            GetFieldData = Trim(Replace(fStr("CTY_DESC"), ",", "")) + IIf(lLastField, "", ",")
        Else
            GetFieldData = ","
        End If
    ElseIf cField = "C_COUNTRY" Then
        Call d4seek(data("country"), fStr(cField))
        GetFieldData = Trim(Replace(fStr("CTRY_DESC"), ",", "")) + IIf(lLastField, "", ",")
    Else
        GetFieldData = Trim(Replace(fStr(cField), ",", "")) + IIf(lLastField, "", ",")
    End If
End Function
Private Sub Form_Unload(Cancel As Integer)
    ' close data
    code4close (cb)
    code4initUndo (cb)
End Sub

Private Function StrZero(n As Long, nLen As Integer)
    StrZero = Right(String(nLen, "0") + Trim(Str(n)), nLen)
End Function
Public Function PadStr(cString As String, nLen As Integer) As String
    PadStr = Left(Trim(cString) + Space(nLen), nLen)
End Function
Private Function fStr(cData As String)
    fStr = f4str(data(cData))
End Function
Private Function IsBlank(cString As String)
    IsBlank = (Len(AllTrim(cString)) = 0)
End Function
Private Function AllTrim(cString As String)
    AllTrim = RTrim(LTrim(cString))
End Function
Private Function GetDDTerm(cFrq As String)
    Select Case Trim(cFrq)
    Case "M"
        GetDDTerm = 1
    Case "Q"
        GetDDTerm = 3
    Case "S"
        GetDDTerm = 6
    Case "A"
        GetDDTerm = 12
    Case "2"
        GetDDTerm = 24
    Case "3"
        GetDDTerm = 36
    Case Else
        GetDDTerm = "99"
    End Select
End Function
Private Function GetIssueDate(cType As String)
    Dim cNextDate As String
    GetIssueDate = ""
    If cType = "End" Then
        If IsBlank(fStr("S_ENDISS")) Then
            ' calc issue before next payment
            Call d4seek(data("pay_proc"), fStr("S_PAY_ID"))
            If Left(fStr("PP_STATUS"), 1) = "A" Then
                ' no future payments scheduled
                'MsgBox "no payment"
            Else
                cNextDate = fStr("PP_DATE")
                Do While fStr("PP_PAYMENT") = fStr("S_PAY_ID") And Left(fStr("PP_STATUS"), 1) <> "A" And d4eof(data("pay_proc")) = 0
                    cNextDate = fStr("PP_DATE")
                    Call d4skip(data("pay_proc"), 1)
                Loop
            End If
            ' get next issue before cNextDate
            If IsBlank(fStr("S_LASTSENT")) Then
                If d4seek(data("issue"), fStr("S_PUBLICAT") + fStr("S_STARTISS")) = 0 Then
                    GetIssueDate = f4date(data("ISS_MAIL"))
                    Do While fStr("S_PUBLICAT") = fStr("ISS_PUB") And fStr("ISS_MAIL") < cNextDate And d4eof(data("issue")) = 0
                        GetIssueDate = f4date(data("ISS_MAIL"))
                        Call d4skip(data("issue"), 1)
                    Loop
                End If
                If GetIssueDate = "" And Not IsBlank(fStr("S_CANCDATE")) Then
                    ' no record of last sent so use cancel date
                    GetIssueDate = f4date(data("S_CANC_DATE"))
                End If
            ElseIf d4seek(data("issue"), fStr("S_PUBLICAT") + fStr("S_LASTSENT")) = 0 Then
                GetIssueDate = f4date(data("ISS_MAIL"))
                Do While fStr("S_PUBLICAT") = fStr("ISS_PUB") And fStr("ISS_MAIL") < cNextDate And d4eof(data("issue")) = 0
                    GetIssueDate = f4date(data("ISS_MAIL"))
                    Call d4skip(data("issue"), 1)
                Loop
            End If
        Else
            If d4seek(data("issue"), fStr("S_PUBLICAT") + fStr("S_ENDISS")) = 0 Then
                GetIssueDate = f4date(data("ISS_MAIL"))
            Else
                GetIssueDate = ""
            End If
        End If
    ElseIf cType = "Start" Then ' start
        If d4seek(data("issue"), fStr("S_PUBLICAT") + fStr("S_STARTISS")) = 0 Then
            GetIssueDate = f4date(data("ISS_MAIL"))
        Else
            GetIssueDate = ""
        End If
    ElseIf cType = "LastSent" Then ' last sent
        If d4seek(data("issue"), fStr("S_PUBLICAT") + fStr("S_LASTSENT")) = 0 Then
            GetIssueDate = f4date(data("ISS_MAIL"))
        Else
            GetIssueDate = ""
        End If
    End If
End Function
Private Function CountIssuesSent()
    Dim nIssues As Integer
    Call d4seek(data("liabmail"), fStr("S_ID"))
    nIssues = 0
    Do While fStr("LM_SUBS") = fStr("S_ID") And d4eof(data("liabmail")) = 0
        If fStr("LM_ISSUE") <> "999999" Then
            nIssues = nIssues + 1
        End If
        Call d4skip(data("liabmail"), 1)
    Loop
    CountIssuesSent = nIssues
End Function
Private Sub SetArchiveTypeList()
    cArchiveTypeList = ""
    Call d4seek(data("subtdigi"), PadStr("ARCHIVE", 12))
    Do While fStr("SUBTD_DIGI") = PadStr("ARCHIVE", 12)
        cArchiveTypeList = cArchiveTypeList + fStr("SUBTD_TYPE") + "~"
        Call d4skip(data("subtdigi"), 1)
    Loop
End Sub
Private Sub SetPrintDigitalTypeList()
    cDigitalTypeList = ""
    cDigitalPlusTypeList = ""
    cBundleTypeList = ""
    d4top (data("sub_type"))
    Do While d4eof(data("sub_type")) = 0
        If fStr("SUBT_DIGIT") = "Y" Then
            If InStr(cArchiveTypeList, fStr("SUBT_TYPE")) = 0 Then
                cDigitalPlusTypeList = cDigitalPlusTypeList + fStr("SUBT_TYPE") + "~"
            Else
                cDigitalTypeList = cDigitalTypeList + fStr("SUBT_TYPE") + "~"
            End If
        End If
        If fStr("SUBT_PRDIG") = "Y" Then
            cBundleTypeList = cBundleTypeList + fStr("SUBT_TYPE") + "~"
        End If
        Call d4skip(data("sub_type"), 1)
    Loop
End Sub
Private Sub SetSubTypes()
    Dim lArchive As Boolean
    lArchive = False
    ' print, digital, archive
    ' print = YNN
    ' digital only = NYN
    ' bundle = YYY
    ' digital plus = NYY
    
    ' set defaults
    aSub(21) = "Y" ' print
    aSub(22) = "N" ' digital
    aSub(26) = "N" ' archive
    If d4seek(data("sub_type"), fStr("S_PUBLICAT") + fStr("S_SUBTYPE")) = 0 Then
        lArchive = (InStr(cArchiveTypeList, fStr("S_SUBTYPE")) > 0)
        If fStr("SUBT_PRDIG") = "Y" Then
            ' bundle
            aSub(21) = "Y"
            aSub(22) = "Y"
            aSub(26) = "Y"
        ElseIf lArchive And fStr("SUBT_DIGIT") = "Y" Then
            aSub(21) = "N"
            aSub(22) = "Y"
            aSub(26) = "Y"
        ElseIf fStr("SUBT_DIGIT") = "Y" Then
            ' digital
            aSub(21) = "N"
            aSub(22) = "Y"
            aSub(26) = "N"
        End If
    End If

End Sub
Private Sub SetSubValues()
    Dim nBundleDigitalPortion As Double, nDigitalPortion As Double
    ' asub(21) = print Y/N
    ' asub(22) = digital Y/N
    ' asub(26) = archive Y/N
    ' asub(23) = print value
    ' asub(24) = digital only value
    ' asub(25) = bundle value
    ' asub(27) = digital plus value
    ' asub(14)= sub value

    Select Case aSub(21) + aSub(22) + aSub(26)
    Case "YNN"
        ' print
        aSub(23) = aSub(14)
        aSub(24) = 0
        aSub(25) = 0
        aSub(27) = 0
    Case "NYN"
        ' digital only
        aSub(23) = 0
        If f4double(data("S_SUBAMT")) = (f4double(data("S_NETT1")) + f4double(data("S_NETT2"))) Then
            ' VAT not applied
            aSub(24) = aSub(14)
        Else
            aSub(24) = Round(aSub(14) / (f4double(data("S_SUBAMT")) / (f4double(data("S_NETT1")) + f4double(data("S_NETT2")))), 2)
        End If
        aSub(25) = 0
        aSub(27) = 0
    Case "YYY"
        ' bundle
        nBundleDigitalPortion = 0
        If d4seek(data("offer"), fStr("S_PUBLICAT") + fStr("S_OFFER")) = 0 Then
            If f4double(data("OFF_AMT")) > 0 Then
                If fStr("OFF_VAT1") = "Z" And fStr("OFF_VAT2") = "Z" Then
                    nBundleDigitalPortion = 0
                ElseIf fStr("OFF_VAT1") = "Z" And fStr("OFF_VAT2") = "S" Then
                    nBundleDigitalPortion = f4double(data("OFF_VALV2")) / f4double(data("OFF_AMT"))
                ElseIf fStr("OFF_VAT1") = "S" And fStr("OFF_VAT2") = "Z" Then
                    nBundleDigitalPortion = f4double(data("OFF_VALV1")) / f4double(data("OFF_AMT"))
                ElseIf fStr("OFF_VAT1") = "S" And fStr("OFF_VAT2") = "S" Then
                    nBundleDigitalPortion = 1
                End If
            End If
            If nBundleDigitalPortion = 0 Then
                ' no split - probably USA/ROW offere so set digital portion to 10%
                nBundleDigitalPortion = 0.1
            End If
        End If
        aSub(24) = 0
        If f4double(data("S_SUBAMT")) = (f4double(data("S_NETT1")) + f4double(data("S_NETT2"))) Then
            ' VAT not applied
            aSub(25) = Round(aSub(14) * nBundleDigitalPortion, 2)
            aSub(23) = aSub(14) - aSub(25)
        Else
            nDigitalPortion = Round(aSub(14) * nBundleDigitalPortion, 2)
            aSub(25) = nDigitalPortion
            If fStr("S_VAT1") = "S" And (f4double(data("S_NETT1")) + f4double(data("S_VATVAL1"))) > 0 Then
                aSub(25) = Round(nDigitalPortion * (f4double(data("S_NETT1")) / (f4double(data("S_NETT1")) + f4double(data("S_VATVAL1")))), 2)
            ElseIf fStr("S_VAT2") = "S" And (f4double(data("S_NETT2")) + f4double(data("S_VATVAL2"))) Then
                aSub(25) = Round(nDigitalPortion * (f4double(data("S_NETT2")) / (f4double(data("S_NETT2")) + f4double(data("S_VATVAL2")))), 2)
            End If
            aSub(23) = aSub(14) - nDigitalPortion
        End If
        aSub(27) = 0
    Case "NYY"
        ' digital+
        aSub(23) = 0
        aSub(24) = 0
        aSub(25) = 0
        'Some subs have negative VAT value ignore these.
        If (f4double(data("S_SUBAMT")) = (f4double(data("S_NETT1")) + f4double(data("S_NETT2")))) Or (f4double(data("S_VATVAL1")) + f4double(data("S_VATVAL2")) < 0) Then
            ' VAT not applied
            aSub(27) = aSub(14)
        Else
            aSub(27) = Round(aSub(14) / (f4double(data("S_SUBAMT")) / (f4double(data("S_NETT1")) + f4double(data("S_NETT2")))), 2)
        End If
    End Select

'    ' if not digital & not print+digital => print value = sub value
'    aSub(23) = IIf(aSub(21) = "N" And aSub(22) = "Y", "0", aSub(14))
'    ' if digital and not print+digital => digital value= sub value unless it's a bolt on upgrade for �2.99 or �5
'    aSub(24) = "0"
'    aSub(25) = "0"
'    If aSub(21) = "N" And aSub(22) = "Y" Then
'        ' digital only sub - value = pay amount less VAT
'        If fStr("SUBT_TYPE") = "DIGUPG" Then
'            aSub(25) = Round(aSub(14) / 1.2, 2)
'        Else
'            aSub(24) = Round(aSub(14) / 1.2, 2)
'        End If
'    End If
'    ' if print+digital => set print & bundle values based on offer
'    'If fStr("SUBT_PRDIG") = "Y" Then
'    If aSub(21) = "Y" And aSub(22) = "Y" Then
'        ' get offer & find Z & S VAT values
'
'        If d4seek(data("offer"), fStr("S_PUBLICAT") + fStr("S_OFFER")) = 0 Then
'            If fStr("OFF_VAT1") = "Z" Then
'                aSub(23) = GetSterlingValue("S_NETT1", fStr("OFF_CURR"))
'            ElseIf fStr("OFF_VAT2") = "Z" Then
'                aSub(23) = GetSterlingValue("S_NETT2", fStr("OFF_CURR"))
'            End If
'            If fStr("OFF_VAT1") = "S" Then
'                aSub(25) = GetSterlingValue("S_NETT1", fStr("OFF_CURR"))
'            ElseIf fStr("OFF_VAT2") = "S" Then
'                aSub(25) = GetSterlingValue("S_NETT2", fStr("OFF_CURR"))
'            End If
'        Else
'            ' get values from sub record
'            If fStr("S_VAT1") = "Z" And fStr("S_VAT2") = "Z" Then
'                aSub(23) = GetSterlingValue("S_NETT1", fStr("S_CURR")) + GetSterlingValue("S_NETT2", fStr("S_CURR"))
'            ElseIf fStr("S_VAT1") = "Z" And fStr("S_VAT2") <> "Z" Then
'                aSub(23) = GetSterlingValue("S_NETT1", fStr("S_CURR"))
'            ElseIf fStr("OFF_VAT2") = "Z" Then
'                aSub(23) = GetSterlingValue("S_NETT2", fStr("S_CURR"))
'            End If
'            If fStr("S_VAT1") = "S" And fStr("S_VAT2") = "S" Then
'                aSub(25) = GetSterlingValue("S_NETT1", fStr("S_CURR")) + GetSterlingValue("S_NETT2", fStr("S_CURR"))
'            ElseIf fStr("S_VAT1") = "S" And fStr("S_VAT2") <> "S" Then
'                aSub(25) = GetSterlingValue("S_NETT1", fStr("S_CURR"))
'            ElseIf fStr("OFF_VAT2") = "S" Then
'                aSub(25) = GetSterlingValue("S_NETT2", fStr("S_CURR"))
'            End If
'        End If
'
'        ' check isn't 1/4 DD etc
'        If (aSub(23) + aSub(25)) > aSub(14) And f4double(data("S_SUBAMT")) > 0 Then
'            aSub(23) = Round(aSub(23) * (aSub(14) / f4double(data("S_SUBAMT"))), 2)
'            aSub(25) = Round(aSub(25) * (aSub(14) / f4double(data("S_SUBAMT"))), 2)
'        End If
'
'    End If

End Sub
Private Function GetSterlingValue(cField As String, cCurr As String)
    If Trim(cCurr) = "UKP" Then
        GetSterlingValue = f4double(data(cField))
    ElseIf Trim(cCurr) = "USD" Then
        GetSterlingValue = Round(f4double(data(cField)) / nUSDRate, 2)
    ElseIf Trim(cCurr) = "EUR" Then
        GetSterlingValue = Round(f4double(data(cField)) / nEURRate, 2)
    Else
        GetSterlingValue = f4double(data(cField))
    End If
End Function

